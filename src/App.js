import React, { Component } from 'react';
import './Components/App.css';
import ItemsToChoose from "./Components/ItemsToChoose";
import OrderDetails from "./Components/Items/OrderDetails";



class App extends Component {
    state = {
        order: {
            hamburger: [],
            cheeseburger: [],
            fries: [],
            coffee: [],
            tea: [],
            cola: []
        },
        total: 0
    };
    price = {
        hamburger: 80,
        cheeseburger: 90,
        fries: 45,
        coffee: 70,
        tea: 50,
        cola: 40
    };

    addItems = (name) => {
        const order = {...this.state.order};
        console.log(order);
        order[name].push(this.price[name]);
        this.setState({order});
        console.log(name);
    }

    cancelItems = (name) => {
        const order = {...this.state.order};
        order[name].pop(this.state.order[name]);
        this.setState({order});
    }

    totalSum = () => {
        let sum = 0;
        const orderCopy = {...this.state.order};
        const keys = Object.keys(orderCopy); // ['hamburger', 'cheeseburger', ...]
        for (let i = 0; i < keys.length; i++){
            let key = keys[i]; // 'hamburger'
            let value = orderCopy[keys[key]]; // 0
            for (let val of orderCopy[key]) {
                sum += val;
            }
        }
        console.log(sum);
        
    }

  render() {
      this.totalSum();
    return (

        <div className="App">
            <div className="OrderDetails"><OrderDetails hamburger={this.state.order.hamburger} cheeseburger={this.state.order.cheeseburger} fries={this.state.order.fries} tea={this.state.order.tea} coffee={this.state.order.coffee} cola={this.state.order.cola} /></div>
            <div className="ItemsToChoose"><ItemsToChoose clickAdd={this.addItems} /></div>
            <div></div>
        </div>
    );
  }
}

export default App;
