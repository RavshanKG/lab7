import React from 'react';

const Hamburger = (props) => {
    return (
        <div onClick={props.click} className="item-hamburger">
            <h4>Hamburger</h4>
            <p>Price: 80 KGS</p>
        </div>
    )
}
export default Hamburger;