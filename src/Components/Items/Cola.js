import React from 'react';

const Cola = (props) => {
    return (
        <div onClick={props.click}>
            <h4>Cola</h4>
            <p>Price: 40 KGS</p>
        </div>
    )
}
export default Cola;