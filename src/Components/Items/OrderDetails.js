import React from 'react';
import Hamburger from './Hamburger';
import Cheeseburger from './CheeseBurger';
import Fries from "./Fries";
import Tea from "./Tea";
import Coffee from "./Coffee";
import Cola from "./Cola";
import Total from "../ItemsToShow/Total";



const orderDetails = (props) => {
    return (
        <div className="">
            <div className='txt-order-details'>Order Details</div>
            <span>{props.name}</span>

            <p>{props.hamburger.map((hamburger, index) => <Hamburger key={index} />)}</p>
            <p>{props.cheeseburger.map((hamburger, index) => <Cheeseburger key={index} />)}</p>
            <p>{props.fries.map((hamburger, index) => <Fries key={index} />)}</p>
            <p>{props.tea.map((hamburger, index) => <Tea key={index} />)}</p>
            <p>{props.coffee.map((hamburger, index) => <Coffee key={index} />)}</p>
            <p>{props.cola.map((hamburger, index) => <Cola key={index} />)}</p>
            <Total/>
        </div>
    )
}
export default orderDetails;