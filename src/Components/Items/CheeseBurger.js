import React from 'react';

const CheeseBurger = (props) => {
    return (
        <div onClick={props.click}>
            <h4>CheeseBurger</h4>
            <p>Price: 90 KGS</p>
        </div>
    )
}
export default CheeseBurger;