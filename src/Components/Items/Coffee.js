import React from 'react';

const Coffee = (props) => {
    return (
        <div onClick={props.click}>
            <h4>Coffee</h4>
            <p>Price: 70 KGS</p>
        </div>
    )
}
export default Coffee