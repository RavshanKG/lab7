import React from 'react';

const Fries = (props) => {
    return (
        <div onClick={props.click}>
            <h4>Fries</h4>
            <p>Price: 45 KGS</p>
        </div>
    )
}
export default Fries;