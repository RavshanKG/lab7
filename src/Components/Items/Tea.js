import React from 'react';

const Tea = (props) => {
    return (
        <div onClick ={props.click}>
            <h4>Tea</h4>
            <p>Price: 70 KGS</p>
        </div>
    )
}
export default Tea;