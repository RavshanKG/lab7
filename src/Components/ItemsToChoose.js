import React from 'react';
import CheeseBurger from "./Items/CheeseBurger";
import Fries from "./Items/Fries";
import Coffee from "./Items/Coffee";
import Tea from "./Items/Tea";
import Cola from "./Items/Cola";
import Hamburger from "./Items/Hamburger";
import "./App.css";

const ItemsToChoose = props => {
    return (
        <div className="">
            <div>
                <div className="item-to-choose-text">Items to Choose</div>
                <button className="btn-hamburger"><Hamburger name="hamburger" click={() => props.clickAdd('hamburger')}  /></button>
                <button className="btn-cheeseburger"><CheeseBurger name="cheeseburger" click={() => props.clickAdd('cheeseburger')}/></button>
                <button className="btn-fries"><Fries name="fries" click={() => props.clickAdd('fries')}/></button>
                <button className="btn-coffee"><Coffee name="coffee" click={() => props.clickAdd('coffee')} /></button>
                <button className="btn-tea"><Tea name="tea" click={() => props.clickAdd('tea')} /></button>
                <button className="btn-cola"><Cola name="cola" click={() => props.clickAdd('cola')} /></button>
            </div>
        </div>
    )

}
export default ItemsToChoose;